## Näidete kasutamine käsurealt (muudatud tekst veebis) ##
### Vaikepaketti kuuluv programm ###
#### Kompileerimine: ####

```
#!bash

javac -cp src src/Ruutvorrand.java
```

#### Käivitamine: ####

```
#!bash

java -cp src Ruutvorrand
```

### Nimega paketti kuuluv programm ###
#### Paketti demo kuuluva programmi kompileerimine: ####

```
#!bash

javac -cp src src/demo/Example.java

```
#### Käivitamine: ####

```
#!bash

java -cp src demo.Example

```
### Testide kasutamine ###
#### Vaikepaketti kuuluva testi kompileerimine: ####

```
#!bash

javac -cp 'src:test:test/junit-4.12.jar:test/hamcrest-core-1.3.jar' test/GradeTest.java

```
Sama Windows aknas (koolonite asemel peavad olema semikoolonid):

```
#!bash

javac -cp 'src;test;test/junit-4.12.jar;test/hamcrest-core-1.3.jar' test/GradeTest.java


```

#### Testi käivitamine: ####

```
#!bash

java -cp 'src:test:test/junit-4.12.jar:test/hamcrest-core-1.3.jar' org.junit.runner.JUnitCore GradeTest
```

Sama Windows aknas (koolonite asemel semikoolonid):

```
#!bash

java -cp 'src;test;test/junit-4.12.jar;test/hamcrest-core-1.3.jar' org.junit.runner.JUnitCore GradeTest
```